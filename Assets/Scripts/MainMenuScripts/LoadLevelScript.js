﻿#pragma strict
import System.Collections.Generic; // trebuie ca sa mearga  List

var ob : GameObject;
var city : GameObject;
var vulcano : GameObject;
var ice : GameObject;
var grass : GameObject;
var Lvls = new List.<GameObject>();  // declaration


function Start () {
	ob = GameObject.Find("Level_Butoane");
	//De aici se incepe blocarea si deblocarea butoanelor
	// aici lv1  == Lvls[0]
	  //      lv2  == Lvls[1]

	for (var i = 1 ;i<21;i++){
	Lvls.Add(GameObject.Find('Lv'+i));
	  
	  }
 for (i = 2 ;i <22;i++) {
 	if (PlayerPrefs.GetInt(i.ToString()) ==0){
 
		Lvls[i-2].GetComponent(Button).interactable = false; 
 	}
 
 }
//	Lvls[0].GetComponent(Button).interactable = false;
Lvls[0].GetComponent(Button).interactable = true; 
Lvls[5].GetComponent(Button).interactable = true; 
Lvls[10].GetComponent(Button).interactable = true; 
Lvls[15].GetComponent(Button).interactable = true; 	
	
	//Aici se termina
	
	city.SetActive(false);
	vulcano.SetActive(false);
	ice.SetActive(false);
	grass.SetActive(false);
	
	

}

function Update () {

}
function City () {
ob.gameObject.SetActive(false);
city.SetActive(true);
}
function Vulcano () {
ob.gameObject.SetActive(false);
vulcano.SetActive(true);
}
function Ice () {
ob.gameObject.SetActive(false);
ice.SetActive(true);
}
function Grass () {
ob.gameObject.SetActive(false);
grass.SetActive(true);
}
function Back () {
ob.gameObject.SetActive(true);
city.SetActive(false);
vulcano.SetActive(false);
ice.SetActive(false);
grass.SetActive(false);
}
function Back_Menu () {
Application.LoadLevel (0);
}